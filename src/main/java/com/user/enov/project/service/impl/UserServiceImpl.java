package com.user.enov.project.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.user.enov.project.entity.UserEntity;
import com.user.enov.project.repository.UserRepository;
import com.user.enov.project.service.UserService;
import com.user.enov.project.shared.dto.AddressDto;
import com.user.enov.project.shared.dto.UserDto;
import com.user.enov.project.shared.util.Util;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	Util utils;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public UserDto createUser(UserDto userDto) {
		
		if(userRepository.findByEmail(userDto.getEmail()) != null) throw new RuntimeException("Record Already exist");
		
		AddressDto address = userDto.getAddress();
		address.setUserDetails(userDto);
		address.setAddressId(utils.generateAddressId(30));
		userDto.setAddress(address);
		
		ModelMapper modelMapper = new ModelMapper();
		UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);
		String publicUserId = utils.generateUserId(30);
		userEntity.setUserId(publicUserId);
		userEntity.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
		UserEntity storedUser = userRepository.save(userEntity);
		
		UserDto returnValue = modelMapper.map(storedUser, UserDto.class);
		return returnValue;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByEmail(username);
		if(userEntity == null) throw new UsernameNotFoundException(username);
		
		return new User(userEntity.getEmail(), userEntity.getPassword(), new ArrayList<>());
	}

	@Override
	public UserDto getUser(String email) {
		UserEntity userEntity = userRepository.findByEmail(email);
		
		if(userEntity == null) throw new UsernameNotFoundException(email);
		ModelMapper modelMapper = new ModelMapper();
		UserDto returnValue = modelMapper.map(userEntity, UserDto.class);
		return returnValue;
	}

	@Override
	public UserDto getUserByUserId(String userId) {
		
		UserEntity userEntity = userRepository.findByUserId(userId);
		if(userEntity == null) throw new UsernameNotFoundException(userId);
		ModelMapper modelMapper = new ModelMapper();
		UserDto returnValue = modelMapper.map(userEntity, UserDto.class);
		return returnValue;
	}

	@Override
	public List<UserDto> getAllUsers() {
		Iterable<UserEntity> userEntity = userRepository.findAll();
		if(userEntity == null) throw new RuntimeException("No Record exist");
		List<UserDto> returnValue = new ArrayList<>();
		for(UserEntity user:userEntity) {
			returnValue.add(new ModelMapper().map(user, UserDto.class));
		}
		return returnValue;
	}

}
