package com.user.enov.project.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user.enov.project.entity.AddressEntity;
import com.user.enov.project.entity.UserEntity;
import com.user.enov.project.repository.AddressRepository;
import com.user.enov.project.repository.UserRepository;
import com.user.enov.project.service.AddressService;
import com.user.enov.project.shared.dto.AddressDto;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AddressRepository addressRepository;
	
	@Override
	public AddressDto getAddress(String userId) {
		
		UserEntity userEntity = userRepository.findByUserId(userId);
		if(userEntity == null) throw new RuntimeException("No Address Record found for userId: "+userId);
		
		AddressDto returnValue = new AddressDto();
		ModelMapper modelMapper = new ModelMapper();
		AddressEntity addressEntity = addressRepository.findByUserDetails(userEntity);
		returnValue = modelMapper.map(addressEntity, AddressDto.class);
		return returnValue;
		
	}

}
