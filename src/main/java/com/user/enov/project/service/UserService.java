 package com.user.enov.project.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.user.enov.project.shared.dto.UserDto;

public interface UserService extends UserDetailsService {
	
	UserDto createUser(UserDto userDto);
	UserDto getUser(String email);
	UserDto getUserByUserId(String userId);
	List<UserDto> getAllUsers();

}
