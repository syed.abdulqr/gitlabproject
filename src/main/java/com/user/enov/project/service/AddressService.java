package com.user.enov.project.service;

import com.user.enov.project.shared.dto.AddressDto;

public interface AddressService {
	AddressDto getAddress(String userId);

}
