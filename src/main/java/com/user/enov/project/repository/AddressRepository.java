package com.user.enov.project.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.user.enov.project.entity.AddressEntity;
import com.user.enov.project.entity.UserEntity;

@Repository
public interface AddressRepository extends CrudRepository<AddressEntity, Long>{
	AddressEntity findByUserDetails(UserEntity userEntity);

}
