package com.user.enov.project.model.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class UserDetailRequestModel {
	
	private String firstName;
	private String lastName;
	@NotBlank(message = "Email is compulsory")
	@NotNull(message = "Email is compulsory")
	private String email;
	@NotBlank
	private String password;
	private String birthday;
	private AddressRequestModel address;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public AddressRequestModel getAddress() {
		return address;
	}
	public void setAddress(AddressRequestModel address) {
		this.address = address;
	}
	
}
