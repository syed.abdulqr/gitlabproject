package com.user.enov.project.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.user.enov.project.model.request.UserLoginRequestModel;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@RestController
public class AuthenticationController {
	
	@ApiOperation("User login")
    @ApiResponses(value = {
    @ApiResponse(code = 200, 
            message = "Response Headers", 
            responseHeaders = {
                @ResponseHeader(name = "authorization", 
                        description = "Bearer <JWT value here>"),
                @ResponseHeader(name = "userId", 
                        description = "<Public User Id value here>")
            })  
    })
	@PostMapping("/user/login")
	public void userLogin(@RequestBody UserLoginRequestModel loginRequestModel) {
		throw new IllegalStateException("This method should not be called. It is implemented by spring security");
	}

}
