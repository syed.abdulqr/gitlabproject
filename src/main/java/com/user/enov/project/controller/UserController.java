package com.user.enov.project.controller;

import java.lang.reflect.Type;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user.enov.project.model.request.UserDetailRequestModel;
import com.user.enov.project.model.response.AddressRest;
import com.user.enov.project.model.response.UserRest;
import com.user.enov.project.service.AddressService;
import com.user.enov.project.service.UserService;
import com.user.enov.project.shared.dto.AddressDto;
import com.user.enov.project.shared.dto.UserDto;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

@RestController
@RequestMapping("users")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	AddressService addressService;
	
	@ApiImplicitParams({
		@ApiImplicitParam(name="authorization", value="${userController.authorizatioHeader.description}", paramType = "header")
	})
	@GetMapping(path = "/{id}")
	public UserRest getUser(@PathVariable String id)
	{
		UserDto userDto = userService.getUserByUserId(id);
		ModelMapper modelMapper = new ModelMapper();
		UserRest returnValue =  modelMapper.map(userDto, UserRest.class);
		return returnValue;
	}
	
	@ApiImplicitParams({
		@ApiImplicitParam(name="authorization", value="${userController.authorizatioHeader.description}", paramType = "header")
	})
	@GetMapping
	public List<UserRest> getAllUser()
	{
		List<UserDto> userDto = userService.getAllUsers();
		
		Type listType = new TypeToken<List<UserRest>>() {}.getType();
		ModelMapper modelMapper = new ModelMapper();
		List<UserRest> returnValue = modelMapper.map(userDto, listType);
		return returnValue;
	}
	
	@PostMapping
	public UserRest createUser(@Valid @RequestBody UserDetailRequestModel userRequest) {
		UserRest returnValue = new UserRest();
		
		ModelMapper modelMapper = new ModelMapper();
		UserDto userDto = modelMapper.map(userRequest, UserDto.class);
		UserDto createdUser = userService.createUser(userDto);
		returnValue = modelMapper.map(createdUser, UserRest.class);
		return returnValue;
	}
	
	@ApiImplicitParams({
		@ApiImplicitParam(name="authorization", value="${userController.authorizatioHeader.description}", paramType = "header")
	})
	@GetMapping(path = "/{id}/address") 
	public AddressRest getUserAddress(@PathVariable String id) {
		
		AddressRest returnValue = new AddressRest();
		ModelMapper modelMapper = new ModelMapper();
		AddressDto addressDto = addressService.getAddress(id);
		returnValue = modelMapper.map(addressDto, AddressRest.class);
		return returnValue;
	}
}
