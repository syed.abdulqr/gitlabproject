package com.user.enov.project.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.user.enov.project.service.UserService;


@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {
	
	private final UserService userDetailsService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public WebSecurity(UserService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userDetailsService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		
		httpSecurity.csrf().disable().authorizeRequests()
		.antMatchers(HttpMethod.POST, "/users")
		.permitAll()
		.antMatchers("/v2/api-docs", "/configuration/**","/swagger-resources/**",  
	            "/swagger-ui.html",  "/swagger*/**", "/webjars/**")
		.permitAll()
		.antMatchers("/actuator/**")
		.permitAll()
		.and()
		.headers().frameOptions().disable()
		.and()
		.authorizeRequests().antMatchers("/h2-console/**").permitAll()		
		.anyRequest().authenticated()
		.and()
		.addFilter(getAuthFilter())
		.addFilter(new AuthorizationFilter(authenticationManager()))
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);;
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
		
	}
	public AutheticationFilter getAuthFilter() throws Exception {
		final AutheticationFilter authFilter = new AutheticationFilter(authenticationManager());
		authFilter.setFilterProcessesUrl("/user/login");
		return authFilter;
	}
}
