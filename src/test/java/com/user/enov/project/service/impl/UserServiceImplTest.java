package com.user.enov.project.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.user.enov.project.entity.UserEntity;
import com.user.enov.project.repository.UserRepository;
import com.user.enov.project.shared.dto.AddressDto;
import com.user.enov.project.shared.dto.UserDto;
import com.user.enov.project.shared.util.Util;

class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl userService;
	
	@Mock
	UserRepository userRepository;
	
	@Mock
	Util utils;
	
	@Mock
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	String userId = "adhkh879";
	String password = "kjsdfh989hj";
	UserEntity userEntity;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		userEntity = new UserEntity();
		userEntity.setId(1L);
		userEntity.setFirstName("syed");
		userEntity.setUserId(userId);
		userEntity.setPassword(password);
	}

	@Test
	void testGetUser() {
		
		when(userRepository.findByEmail(anyString())).thenReturn(userEntity);
		
		UserDto userDto = userService.getUser("test@gmail.com");
		assertNotNull(userDto);
		assertEquals("syed", userDto.getFirstName());
		
	}
	
	@Test
	void testGetUser_UsernameNotFoundException() {
		when(userRepository.findByEmail(anyString())).thenReturn(null);
		assertThrows(UsernameNotFoundException.class,
				
				()-> {
					userService.getUser("test@gmail.com");
				}
				);
		
	}
	@Test
	void testCreateUser() {
		
		when(userRepository.findByEmail(anyString())).thenReturn(null);
		when(utils.generateAddressId(anyInt())).thenReturn("sjshfk8977");
		when(utils.generateUserId(anyInt())).thenReturn(userId);
		when(bCryptPasswordEncoder.encode(anyString())).thenReturn(password);
		
		when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);
		
		AddressDto address = new AddressDto();
		address.setCity("Rotterdam");
		
		UserDto userDto = new UserDto();
		userDto.setAddress(address);
		
		UserDto storeUserDetails = userService.createUser(userDto);
		assertNotNull(storeUserDetails);
		assertEquals(userEntity.getFirstName(), storeUserDetails.getFirstName());
	}

}
