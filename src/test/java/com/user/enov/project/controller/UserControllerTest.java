package com.user.enov.project.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.user.enov.project.model.response.UserRest;
import com.user.enov.project.service.impl.UserServiceImpl;
import com.user.enov.project.shared.dto.AddressDto;
import com.user.enov.project.shared.dto.UserDto;

class UserControllerTest {

	@InjectMocks
	UserController userController;

	@Mock
	UserServiceImpl userService;

	UserDto userDto;
	final String USER_ID = "fsdhg8776fsk675";

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		userDto = new UserDto();
		userDto.setFirstName("Name");
		userDto.setEmail("junit@gmail.com");
		userDto.setUserId(USER_ID);
		userDto.setPassword("jhg89769bgj");
		userDto.setAddress(getAddressDto());
	}

	@Test
	void testGetUser() {
		when(userService.getUserByUserId(anyString())).thenReturn(userDto);
		
		UserRest userRest = userController.getUser(USER_ID);
		
		assertNotNull(userRest);
		assertEquals(USER_ID, userRest.getUserId());
		assertEquals(userDto.getFirstName(), userRest.getFirstName());
		assertTrue(userDto.getAddress().getCity() == userRest.getAddress().getCity());
	}

	private AddressDto getAddressDto() {
		AddressDto addressDto = new AddressDto();
		addressDto.setCity("RM");
		addressDto.setCountry("NL");
		addressDto.setPlace("coolhaven");
		addressDto.setPostalCode("3024");
		return addressDto;
	}
}
